#!/bin/sh

# ---------------------------------------------------------------------
# Script to check important ports and processes and send mail if 
# port is closed or process not running
#
# Copyright (c) 2009 Thomas Wenzel
# ---------------------------------------------------------------------

#### THE FOLLOWING SECTION CAN BE MODIFIED
SENDMAIL=0                         # if 1 a mail would be send
MAILTO="mail@mail.de"              # where to send info
APACHEPORT="80"                    # Apache port
TOMCATPORT="8080"                  # Tomcat port
PROCESS="Process_Name"             # Process name to check
HPPT_APP_PATH="/www/examples"      # path to Servlet 
PATH_TO_LOG="/usr/local/log"       # path to logfile

#### DO NOT MAKE MODIFICATIONS ON THE SECTION BELOW
## Check process is running       
if [ ! "$(pidof $PROCESS)" ]; then
	echo "`Date` -- WARNING! Process $PROCESS not running!" >> $PATH_TO_LOG/server_problem.txt
	SENDMAIL=1
else
	echo "`Date` -- $PROCESS successfully running." >> $PATH_TO_LOG/SimpleProcessChecker.log
fi

## Check port for apache is open
PORTTEST1 = netstat -p tcp -ano | grep -w $APACHEPORT
PORTTEST2 = $PORTTEST1 | grep -w Connection
if [ "$PORTTEST2" -ne "Connection" ]; then #Ok
	echo "`Date` -- $APACHEPORT successfully opened." >> $PATH_TO_LOG/SimpleProcessChecker.log
else #Connection failure
	echo "`Date` -- WARNING! Port $APACHEPORT not open!" >> $PATH_TO_LOG/server_problem.txt
	SENDMAIL=1
fi

## Check port for tomcat is open
PORTTEST1 = netstat -p tcp -ano | grep -w $TOMCATPORT
PORTTEST2 = $PORTTEST1 | grep -w Connection
if [ "$PORTTEST2" -ne "Connection" ]; then #Ok
	echo "`Date` -- $TOMCATPORT successfully opened." >> $PATH_TO_LOG/SimpleProcessChecker.log
else #Connection failure
	echo "`Date` -- WARNING! Port $TOMCATPORT not open!" >> $PATH_TO_LOG/server_problem.txt
	SENDMAIL=1
fi

## Send mail notification if a check causes problem (SENDMAIL = 1)
if [ $SENDMAIL=="1" ]; then
  mail -s "Server problem!" $MAILTO < $PATH_TO_LOG/server_problem.txt
fi
